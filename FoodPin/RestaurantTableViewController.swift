//
//  RestaurantTableViewController.swift
//  FoodPin
//
//  Created by Aleksey Kabishau on 0629..17.
//  Copyright © 2017 Dreamline. All rights reserved.
//

import UIKit

class RestaurantTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        
         // modifing the back button of navigation item
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        // setting self-sizing cells
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // hiding navigation controller every time when view appears
        navigationController?.hidesBarsOnSwipe = true
    }


    // MARK: - Data Source Protocol methods

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        
        // downcastion UITableViewCell to RestaurantTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! RestaurantTableViewCell
        
        cell.nameLabel.text = restaurants[indexPath.row].name
        cell.locationLabel.text = restaurants[indexPath.row].location
        cell.typeLabel.text = restaurants[indexPath.row].type
        cell.thumbnailImageView.image = UIImage(named: restaurants[indexPath.row].image)
        cell.thumbnailImageView.layer.cornerRadius = 10
        cell.thumbnailImageView.clipsToBounds = true
        
        // referring to the array for displaying checkmark or not
        cell.accessoryType = restaurants[indexPath.row].isVisited ? .checkmark : .none
        
        
        return cell
    }
    
    // method tableView(_:commit:forRowAt:) should be JUST existed for displaying "delete" button when user swapes across the row - feature "swape-to-delete". this method is okay when you need just delete and inscert functionality.
    
    /*
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        // removing data from the data source arrays
        if editingStyle == .delete {
            restaurants.remove(at: indexPath.row)
        }
        
        // reloading the Table View
//        tableView.reloadData()
        // removing row from TM without reloading the whole view
        tableView.deleteRows(at: [indexPath], with: .fade)
        
        // just for debugging - to make sure that data is deleted from array
        print("There are \(restaurants.count) in the array")
        
    }
    */
    
    
    
    // adding additional custom actions to row functionalyty - method tableView(_:editActionsForRowAt:)
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        // share button - style, title and code that should be executed
        let shareAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Share", handler: { (action, indexPath) -> Void in
            
            // creating instance of the message that I want to share
            let defaultText = "Just checked in \(restaurants[indexPath.row].name)"
            
            // creating instance of image and unwraping it
            if let imageToShare = UIImage(named: restaurants[indexPath.row].image) {
                
                // creating instance of UIActivityViewController
                let activityController = UIActivityViewController(activityItems: [defaultText, imageToShare], applicationActivities: nil)
                
                // presenting instance of UIActivityViewController on the screen
                self.present(activityController, animated: true, completion: nil)
            }

        })
        
        
        
        // delete button - style, title and code for execution
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Delete", handler: { (action, indexPath) -> Void in
            
            //deletion of the row from data source array
            restaurants.remove(at: indexPath.row)

            
            // deletion of row from the View
            self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            
            // just for debugging purposes
            print("There are \(restaurants.count) in the array")
            
        })
        
        // changing colors of share and delete buttons
        shareAction.backgroundColor = UIColor(red: 48/255, green: 173/255, blue: 99/255, alpha: 1.0)
        deleteAction.backgroundColor = UIColor(red: 202/255, green: 202/255, blue: 203/255, alpha: 1.0)
        
        // return the array of objects - table view should create them when user swipe the row
        return [shareAction, deleteAction]
    }
    
    
    
    //MARK: Delegate Protocol methods
    /*
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // option menu as an action sheet
        let optionMenu = UIAlertController(title: nil, message: "What do you want to do?", preferredStyle: .actionSheet)
        
        // adding cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        // defining handler for call action
        let callActionHandler = { (action: UIAlertAction!) -> Void in
            let alertMessage = UIAlertController(title: "Service Unavailable", message: "Sorry, the call feature is not available yet. Please retry later.", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)
        }
        
        // adding call action
        let callAction = UIAlertAction(title: "Call " + "123-000-\(indexPath.row)", style: .default, handler: callActionHandler)
        
        // adding checkIn feature
        let checkInAction = UIAlertAction(title: "Check In", style: .default, handler: { (action: UIAlertAction!) -> Void in
            let cell = tableView.cellForRow(at: indexPath)
            cell?.accessoryType = .checkmark
            self.restaurantIsVisited[indexPath.row] = true
        })
        
        // adding checkOut action
        
        let checkOutAction = UIAlertAction(title: "Check Out", style: .default, handler: {
            (action: UIAlertAction!) -> Void in
            let cell = tableView.cellForRow(at: indexPath)
            cell?.accessoryType = .none
            self.restaurantIsVisited[indexPath.row] = false
        
        })
        
        // actions to the menu
        optionMenu.addAction(cancelAction)
        optionMenu.addAction(callAction)
        
        // checkIn or checkOut actions are showing depending of current value of array isVisited
        if restaurantIsVisited[indexPath.row] {
            optionMenu.addAction(checkOutAction)
        } else {
            optionMenu.addAction(checkInAction)
        }

        
        // displaying the menu
        present(optionMenu, animated: true, completion: nil)
        
        // unselection selected table row (gray color)
        tableView.deselectRow(at: indexPath, animated: false)

        
    }
    */
    
    //MARK: Segue methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showRestaurantDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                
                let destinationController = segue.destination as! RestaurantDetailViewController
                destinationController.restaurant = restaurants[indexPath.row]
               
            }
        }
    }
    
}
