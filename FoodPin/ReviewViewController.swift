//
//  ReviewViewController.swift
//  FoodPin
//
//  Created by Aleksey Kabishau on 0712..17.
//  Copyright © 2017 Dreamline. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {
    
    @IBOutlet var backgroundImageView: UIImageView!
    
    // variable for transformation of the view
    @IBOutlet var containerView: UIView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // blurring effect of background image
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        blurEffectView.frame = view.bounds
        backgroundImageView.addSubview(blurEffectView)
        
        
        // transformation effect from zero to original size
        
        // this down the view when it is intially loaded
        containerView.transform = CGAffineTransform.init(scaleX: 0, y: 0)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // growing effect of the view after in intially loaded in viewDidLoad method
        UIView.animate(withDuration: 0.3, animations: {
            // this set view to original size and position
            self.containerView.transform = CGAffineTransform.identity
        })
        
        // spring velocity effect example
        /*
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.2, options: .curveEaseInOut, animations: {
            self.containerView.transform = CGAffineTransform.identity
        }, completion: nil)
        */
        
    }

}
